<?php

namespace App\Http\Controllers;

use App\Resident;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResidentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function show(Resident $resident)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function edit(Resident $resident)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resident $resident)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resident $resident)
    {
        //
    }
    public function add(Request $request)
    {
          $validator = Validator::make($request->all(),[
              'name' => 'required|max:255',

                   'small_text' =>'required',
                   'text' =>'required',
          ]);
          if($validator->fails())
          {
            return redirect('/Residents')
            ->withInput()
            ->withErrors($validator);
          }
          if( $request->type=='on')
          {
                 $request->show=1;
          }
          else {
                 $request->show=0;
          }
            $resident =new \App\Resident;
            $resident->name= $request->name;

            $resident->small_text= $request->small_text;
            $resident->text= $request->text;

            $resident->save();

            return redirect('/Residents');
    }
    public function red(Request $request, Resident $resident)
    {      $resident->delete();

          $validator = Validator::make($request->all(),[
              'name' => 'required|max:255',

                   'small_text' =>'required',
                   'text' =>'required',
             //'show'=>'required',
          ]);
          if($validator->fails())
          {
            return redirect('/Residents')
            ->withInput()
            ->withErrors($validator);
          }
          // if( $request->type=='on')
          // {
          //        $request->show=1;
          // }
          // else {
          //        $request->show=0;
          // }
            $resident =new \App\Resident;
            $resident->name= $request->name;

            $resident->small_text= $request->small_text;
            $resident->text= $request->text;
          // $resident->show_news= $request->show;
            $resident->save();

            return redirect('/Residents');
    }
}
