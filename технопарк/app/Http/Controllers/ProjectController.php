<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }
    public function add(Request $request)
    {
          $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',

             'small_text' =>'required',
             'text' =>'required',

          ]);
          if($validator->fails())
          {
            return redirect('/Projects')
            ->withInput()
            ->withErrors($validator);
          }
          if( $request->type=='on')
          {
                 $request->show=1;
          }
          else {
                 $request->show=0;
          }
            $project =new \App\Project;
            $project->name= $request->name;

            $project->small_text= $request->small_text;
            $project->text= $request->text;

            $project->save();

            return redirect('/Projects');
    }
    public function red(Request $request, Project $project)
    {
          $project->delete();

          $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',

            'small_text' =>'required',
            'text' =>'required',

             //'show'=>'required',
          ]);
          if($validator->fails())
          {
            return redirect('/Projects')
            ->withInput()
            ->withErrors($validator);
          }
          // if( $request->type=='on')
          // {
          //        $request->show=1;
          // }
          // else {
          //        $request->show=0;
          // }
            $project =new \App\Project;
            $project->name= $request->name;

            $project->small_text= $request->small_text;
            $project->text= $request->text;

          // $project->show_news= $request->show;
            $project->save();

            return redirect('/Projects');
    }
}
