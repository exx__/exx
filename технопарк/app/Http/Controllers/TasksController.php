<?php

namespace App\Http\Controllers;


use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TasksController extends Controller
{
  // public function index()
  // {
  //     $tasks=\App\Task::orderBy('created_at', 'asc')->get();
  //     return view('tasks', ['tasks'=>$tasks]);
  // }
  public function index()
  {
    return redirect('/login');
  }
  public function vhod()
  {
    return redirect('/menu');
  }
  public function add(Request $request)
  {
        $validator = Validator::make($request->all(),[
          'name' => 'required|max:255'
        ]);
        if($validator->fails())
        {
          return redirect('/')
          ->withInput()
          ->withErrors($validator);
        }
          $task =new \App\Task;
          $task->name= $request->name;
          $task->save();

          return redirect('/');
  }
  public function delete(Task $task)
  {
        $task->delete();
        return redirect('/');
  }
}
