<?php

namespace App\Http\Controllers;

use App\Tiding;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TidingController extends Controller
{
      public function add(Request $request)
      {
            $validator = Validator::make($request->all(),[
              'name' => 'required|max:255',
              'date' =>'required',
               'small_text' =>'required',
               'text' =>'required',
             'type'=>'required|in:"Прес-центр", "Мероприятия"',
             'show'=>'required',
            ]);
            if($validator->fails())
            {
              return redirect('/menu')
              ->withInput()
              ->withErrors($validator);
            }
            if( $request->type=='on')
            {
                   $request->show=1;
            }
            else {
                   $request->show=0;
            }
              $tiding =new \App\Tiding;
              $tiding->name= $request->name;
              $tiding->date= $request->date;
              $tiding->small_text= $request->small_text;
              $tiding->text= $request->text;
             $tiding->type= $request->type;
             $tiding->show_news= $request->show;
              $tiding->save();

              return redirect('/menu');
      }
      public function red(Request $request, Tiding $tiding)
      {      $tiding->delete();

            $validator = Validator::make($request->all(),[
              'name' => 'required|max:255',
              'date' =>'required',
              'small_text' =>'required',
              'text' =>'required',
              'type'=>'required|in:"Прес-центр", "Мероприятия"',
               //'show'=>'required',
            ]);
            if($validator->fails())
            {
              return redirect('/menu')
              ->withInput()
              ->withErrors($validator);
            }
            // if( $request->type=='on')
            // {
            //        $request->show=1;
            // }
            // else {
            //        $request->show=0;
            // }
              $tiding =new \App\Tiding;
              $tiding->name= $request->name;
              $tiding->date= $request->date;
              $tiding->small_text= $request->small_text;
              $tiding->text= $request->text;
             $tiding->type= $request->type;
            // $tiding->show_news= $request->show;
              $tiding->save();

              return redirect('/menu');
      }
}
