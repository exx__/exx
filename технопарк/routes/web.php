<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \App\Tiding;
use \App\Resident;
use \App\Project;
use \Illuminate\Http\Request;

Route::get('/', 'TasksController@index');
Route::post('/news_add', 'TidingController@add');
Route::post('/news_red/{tiding}', 'TidingController@red');
Route::post('/resident_red/{resident}', 'ResidentController@red');
Route::post('/resident_add', 'ResidentController@add');
Route::post('/project_red/{project}', 'ProjectController@red');
Route::post('/project_add', 'ProjectController@add');
Route::post('/vhod', 'TasksController@vhod');
Route::delete('/task/{task}/delete', 'TasksController@delete');
//Route::get('/tasks', 'TasksController@show');

// Route::get('/Projects', function () {
//     if (session('resent'))
//  return redirect('Projects');
//  else return redirect('/');
// });
Route::get('/Residents', function () {
  $resident=\App\Resident::orderBy('created_at', 'asc')->get();
 return view('Residents', ['residents'=>$resident, 'i'=>1]);
});
Route::get('/Projects', function () {
  $project=\App\Project::orderBy('created_at', 'asc')->get();
 return view('Projects', ['projects'=>$project, 'i'=>1]);
});
// Route::get('/menu', function () {
//  return view('menu');
// });
Route::get('/menu', function () {
    $tidings=\App\Tiding::orderBy('created_at', 'asc')->get();
   return view('menu', ['tidings'=>$tidings, 'i'=>1]);
 //return view('menu');
});

Route::get('/red-news', function () {
 return view('red-news');
});
Route::get('/Resident_new', function () {
 return view('Resident_new');
});
Route::get('/Project_new', function () {
 return view('Project_new');
});

// Route::post('/task', function (Request $request) {
//     $validator = Validator::make($request->all(),[
//       'name' => 'required|max:255'
//     ]);
//     if($validator->fails())
//     {
//       return redirect('/')
//       ->withInput()
//       ->withErrors($validator);
//     }
//   $task =new \App\Task;
//   $task->name= $request->name;
//   $task->save();
//
//   return redirect('/');
// });
//
Route::delete('/project/{project}/delete', function (Project $project) {
  $project->delete();
  return redirect('/Projects');
});
Route::post('/project/{project}/red', function(Project $project) {
 return view('Project_new', ['project'=>$project]);
});
//\
//\
//\
Route::delete('/menu/{tiding}/delete', function (Tiding $tiding) {
  $tiding->delete();
  return redirect('/menu');
});
Route::post('/menu/{tiding}/red', function(Tiding $tiding) {
 return view('red-news', ['tiding'=>$tiding]);
});
Route::delete('/resident/{resident}/delete', function (Resident $resident) {
  $resident->delete();
  return redirect('/Residents');
});
Route::post('/resident/{resident}/red', function(Resident $resident) {
 return view('Resident_new', ['resident'=>$resident]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
