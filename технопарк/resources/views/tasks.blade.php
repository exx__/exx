@extends('layouts.app')

@section('content')

<div class="card-body">
@include('errors')

<form action="{{url('task')}}" method="POST" class="form-horizontal">
	{{csrf_field() }}

	<div class="row">
		<div class="form-group">
			<label for="task"  class="col-sm-3 control-label">task</label>
			<div class="row">
				<div class="col-sm-6">
					<input type="text" name="name" id="task-name" class="for-control">
				</div>
				<div class="col-sm-6">
					<button type="submit" class="btn btn-success" name="button"><i class="fa fa-plus"></i> add task</button>
				</div>
			</div>
		</div>
	</div>
</form>
</div>


@if(count($tasks)>0)
<div class="card">
	<div class="card-heading">
		Current tasks
	</div>
	<div class="card-body">
		<table class="table table-striped task-table">
			<thead>
				<th>task</th>
				<th>&nbsp;</th>
			</thead>
			<tbdoy>
				@foreach($tasks as $task)
				<td class="table-text">
						<div class="">
							{{$task->name}}
						</div>
				</td>
				<td>

					 <form class="" action="{{ url('task/'.$task->id.'/delete') }}" method="post">
							{{csrf_field()}}
							{{method_field('DELETE')}}
							<button type="submit" class="btn btn-danger" name="button">
								Delete
							</button>
						</form>
				</td>
				@endforeach
			</tbdoy>
		</table>
	</div>
</div>
@endif
@endsection
