<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Админка</title>
  <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
  <section class="news">
      <?php
        require('php/header.php')
      ?>

    <h2 class="title-page">Резиденты</h2>
@if(isset($resident))
      <form action="{{url('resident_red/'.$resident->id.'')}}" method="POST" class="form-horizontal">
        {{csrf_field() }}

      <p class="form-horizontal-text">Показать:<input type="checkbox" class="chekked" name="show" id="show" checked></p>


      </select>
      </p><br>
      <p class="form-horizontal-text">Заголовок:
      <input type="text" class="form-horizontal-input" id="name" name="name" value={{$resident->name}}>
      </p><br>
      <p class="form-horizontal-text">Краткий текст:
      <input class="form-horizontal-input" type="text" name="small_text" id="small_text" value={{$resident->small_text}}>
      </p><br>
      <p class="form-horizontal-text">Текст:
      <textarea class="form-horizontal-input" id="text" name="text">
        {{$resident->text}}
      </textarea>
      </p><br>
      <label>
      <input type="file" name="newphoto" id="photo" >
        </label>
        <button type="submit" class="bot8"  > Сохранить</button>
      </form>

  @else
      <form action="{{url('resident_add')}}" method="POST" class="form-horizontal">
      	{{csrf_field() }}

    <p class="form-horizontal-text">Показать:<input type="checkbox" class="chekked" name="show" id="show" checked></p>
    </p><br>
    <p class="form-horizontal-text">Заголовок:
      <input type="text" class="form-horizontal-input" id="name" name="name" value="">
    </p><br>
    <p class="form-horizontal-text">Краткий текст:
      <input class="form-horizontal-input" type="text" name="small_text" id="small_text" value="">
    </p><br>
    <p class="form-horizontal-text">Текст:
    <textarea class="form-horizontal-input" id="text" name="text">
    </textarea>
    </p><br>
    <label>
      <input type="file" name="newphoto" id="photo" >
        </label>
        <button type="submit" class="bot8"  > Сохранить</button>
      </form>

@endif
</body>
</html>
