<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Админка</title>
  <link rel="stylesheet" href="css/style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap&subset=cyrillic" rel="stylesheet">
</head>
<body>


  <section class="popup-reg">
    <div class="popup-reg__block">
      <img class="popup-reg__icons" src="img/logo.png" alt="">
      <p class="popup-reg__title">Панель администратора</p>
    </div>
<form method="POST" action="{{ route('login') }}">
    <div class="popup-reg__block" >
      <p class="popup-reg__title-input">Логин:</p>
      <input class="popup-reg__input" id="email" name="email" required autocomplete="email" autofocus type="text">
      @error('email')
    </div>

    <div class="popup-reg__block" >
      <p class="popup-reg__title-input">Пароль:</p>
      <input class="popup-reg__input" type="password">
    </div>

    <input id="password" type="password" name="password" required autocomplete="current-password" class="popup-reg__btn" type="submit" value="Войти">

  </section>

</body>
</html>
