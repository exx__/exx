<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Админка</title>
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <section class="news">
      <?php
        require('php/header.php')
      ?>

      <h2 class="title-page">Проекты</h2>
<a class="bot8"  href="/Project_new">Добавить</a>
@if(count($projects)>0)

      <div class="info-menu">

              <p class="info-menu__number">№</p>
              <p class="info-menu__titel">Заголовок</p>
              

      </div>
      @foreach($projects as $project)
            <div class="info-menu">

                <p class="info-menu__number">{{$i++}}</p>
                <p class="info-menu__titel">	{{$project->name}}</p>


                <img class="info-menu__icons" src="img/lamp.svg" alt="">

                <!-- <img class="info-menu__icons" src="img/cancel.svg" alt=""> -->
                <form class="" action="{{ url('project/'.$project->id.'/delete') }}" method="post">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button  type="submit"  class="btn btn-medium">
                     <i class="icon-excel-del" ></i>
                 </button>
               </form>
               <form class="" action="{{ url('project/'.$project->id.'/red') }}" method="post">
               {{csrf_field()}}

               <button type="submit" class="btn btn-medium">
                    <i class="icon-excel" ></i>
                </button>
                </form>
          </div>

      		@endforeach
          @endif
  </section>


</body>
</html>
