<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Админка</title>
  <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
  <section class="news">
      <?php
        require('php/header.php')
      ?>

      <h2 class="title-page">Проекты</h2>
      {{$project->id}}
@if(isset($project))
      <form action="{{url('project_red/'.$project->id.'')}}" method="POST" class="form-horizontal">
        {{csrf_field() }}

      <p class="form-horizontal-text">Показать:<input type="checkbox" class="chekked" name="show" id="show" checked></p>

      </p><br>
      <p class="form-horizontal-text">Заголовок:
      <input type="text" class="form-horizontal-input" id="name" name="name" value={{$project->name}}>
      </p><br>
      <p class="form-horizontal-text">Краткий текст:
      <input class="form-horizontal-input" type="text" name="small_text" id="small_text" value={{$project->small_text}}>
      </p><br>
      <p class="form-horizontal-text">Текст:
      <textarea class="form-horizontal-input" id="text" name="text">
        {{$project->text}}
      </textarea>
      </p><br>

        <button type="submit" class="bot8"  > Сохранить</button>
      </form>

  @else
      <form action="{{url('project_add')}}" method="POST" class="form-horizontal">
      	{{csrf_field() }}

    <p class="form-horizontal-text">Показать:<input type="checkbox" class="chekked" name="show" id="show" checked></p>

    </p><br>
    <p class="form-horizontal-text">Заголовок:
      <input type="text" class="form-horizontal-input" id="name" name="name" value="">
    </p><br>
    <p class="form-horizontal-text">Краткий текст:
      <input class="form-horizontal-input" type="text" name="small_text" id="small_text" value="">
    </p><br>
    <p class="form-horizontal-text">Текст:
    <textarea class="form-horizontal-input" id="text" name="text">
    </textarea>
    </p><br>

        <button type="submit" class="bot8"  > Сохранить</button>
      </form>

@endif
</body>
</html>
