<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Админка</title>
  <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
  <section class="news">
      <?php
        require('php/header.php')
      ?>

      <h2 class="title-page">Новости и мероприятия</h2>
@if(isset($tiding))
      <form action="{{url('news_red/'.$tiding->id.'')}}" method="POST" class="form-horizontal">
        {{csrf_field() }}

      <p class="form-horizontal-text">Показать:<input type="checkbox" class="chekked" name="show" id="show" checked></p>
      <p class="form-horizontal-text">Выберите дату: <input type="date" name="date" id="date" value={{$tiding->date}}></p><br>
      <p class="form-horizontal-text"> Категория:
      <select id="type" name="type">
        <!-- <option selected disabled  value="{{$tiding->type}}">{{$tiding->type}}</option> --> -->
        <option value="Прес-центр">Прес-центр</option>
        <option value="Мероприятия">Мероприятия</option>

      </select>
      </p><br>
      <p class="form-horizontal-text">Заголовок:
      <input type="text" class="form-horizontal-input" id="name" name="name" value={{$tiding->name}}>
      </p><br>
      <p class="form-horizontal-text">Краткий текст:
      <input class="form-horizontal-input" type="text" name="small_text" id="small_text" value={{$tiding->small_text}}>
      </p><br>
      <p class="form-horizontal-text">Текст:
      <textarea class="form-horizontal-input" id="text" name="text">
        {{$tiding->text}}
      </textarea>
      </p><br>
      <label>
      <input type="file" name="newphoto" id="photo" >
        </label>
        <button type="submit" class="bot8"  > Сохранить</button>
      </form>

  @else
      <form action="{{url('news_add')}}" method="POST" class="form-horizontal">
      	{{csrf_field() }}

    <p class="form-horizontal-text">Показать:<input type="checkbox" class="chekked" name="show" id="show" checked></p>
      <p class="form-horizontal-text">Выберите дату: <input type="date" name="date" id="date" value="2019-07-07"></p><br>
      <p class="form-horizontal-text"> Категория:
      <select id="type" name="type">
        <option value="Прес-центр">Прес-центр</option>
        <option value="Мероприятия">Мероприятия</option>
      </select>
    </p><br>
    <p class="form-horizontal-text">Заголовок:
      <input type="text" class="form-horizontal-input" id="name" name="name" value="">
    </p><br>
    <p class="form-horizontal-text">Краткий текст:
      <input class="form-horizontal-input" type="text" name="small_text" id="small_text" value="">
    </p><br>
    <p class="form-horizontal-text">Текст:
    <textarea class="form-horizontal-input" id="text" name="text">
    </textarea>
    </p><br>
    <label>
      <input type="file" name="newphoto" id="photo" >
        </label>
        <button type="submit" class="bot8"  > Сохранить</button>
      </form>

@endif
</body>
</html>
