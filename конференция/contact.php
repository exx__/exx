<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<title>Наши услуги</title>
	<?php
         	require ('php/head.php');
    ?>
</head>

<body>

	<?php
         	require ('php/header.php');
    ?>

	<section class="wrap-content" id="contact" >
		<h2 class="wrap-content__title">Контакты</h2>


		<div class="wrap-content__map">

			<img class="map__img" src="img/map.png" alt="">

			<div class="map__block-contact">
				<div class="contact__block">
					<p class="block__title">Наш адрес</p>
					<p class="block__info">Адрес</p>
				</div>

				<div class="contact__block">
					<p class="block__title">Телефон</p>
					<p class="block__info">8 (888) 888-88-88</p>
				</div>

				<div class="contact__block">
					<p class="block__title">E-mail</p>
					<p class="block__info">info@email.com</p>
				</div>
				
				<a class="wrap-content__about" href="phone.php">Связаться с нами</a>
			</div>
			
			
		</div>





		<div class="bottom-info__right-block">
			<p class="right-block__text contack-title">Контакты</p>
			<p class="right-block__num">07</p>
			<img src="img/line.png" alt="">
		</div>
	</section>








	<?php
         	require ('php/footer.php');
    ?>

</body>

</html>