<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<title>Чем мы занимаемся?</title>
	<?php
         	require ('php/head.php');
    ?>
</head>

<body>

	<?php
         	require ('php/header.php');
    ?>

	<section class="wrap-content" id="about" >
		<h2 class="wrap-content__title">Чем мы занимаемся?</h2>
		
		<p class="wrap-content__sub-title">Сообщество Молодежного бизнес-инкубатора ЛГТУ. В нашей группе вы сможете найти помощь по вопросам развития вашего бизнеса, а также контактные данные множества специалистов, и организаций по разным профилям консалтинговой деятельности. </p>
		
		<ul class="content-ul">
			<li class="ul-item">Бизнес-план;</li>
			<li class="ul-item">Стратегию развития организации;</li>
			<li class="ul-item">Финансовый план;</li>
			<li class="ul-item">Оценку рынка, маркетинговый план;</li>
			<li class="ul-item">Медиаплан;</li>
			<li class="ul-item">И многое, многое другое.</li>
		</ul>
		
		
		<a class="wrap-content__about" href="phone.php">Связаться с нами</a>	
	</section>








	<?php
         	require ('php/footer.php');
    ?>
    
    	<div class="bottom-info__right-block">
			<p class="right-block__text about-title">О нас</p>
			<p class="right-block__num">02</p>
			<img src="img/line.png" alt="">
		</div>
		
		<img class="bg-girl" src="img/girl.png" alt="">

</body>

</html>