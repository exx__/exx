<?php   session_start();
  if(isset($_SESSION['login'])) {

header('Location: ../admin.php');
  }
    ?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="../css/admin.css"> <!-- Подключаем внешнюю таблицу стилей -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/ajax.js"></script>
    <!--script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script-->


    <noscript>
        <link rel="stylesheet" type="text/css" href="css/noJS.css" /></noscript>
    <!--[if lte IE 7]><style>.main{display:none;} .support-note .note-ie{display:block;}</style><![endif]-->
</head>
<body>

  <main class="main-content-wrapper">
    <div class="popup-reg">
       <h6 class="popup-reg-title">Войти на сайт</h6>
       <form action="" onsubmit="return false;" class="form__input">
           <input type="login" class="input__pole" placeholder="Логин" id="login" name="login" required>

           <input type="password" class="input__pole" placeholder="Пароль" id="Password" name="Password" required>
       </form>


        <div class="popup-reg-btn-sabmit">
           <button type="submit" id="btn_vhod" class="btm-sub">Вход</button>
       </div>
    </div>
</main>
</body>

    </html>
