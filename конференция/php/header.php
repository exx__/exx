<header class="header">
	
	<a href="index.php" class="header__link">МБИ<br><span>ЛГТУ</span></a>
	
	<nav>
		<ul class="nav__list">
		
			<li><a class="list__item" id="desk-index" href="../index.php">Главная</a></li>
			
			<li><a class="list__item" id="desk-adout" href="../about.php">О нас</a></li>
			
			<li><a class="list__item" id="desk-parther" href="../partner.php">Партнеры</a></li>
			
			<li><a class="list__item" id="desk-prace" href="../service.php">Услуги</a></li>
			
			<li><a class="list__item" id="desk-docs" href="../docs.php">Документы</a></li>
			
			<li><a class="list__item" id="desk-news" href="../news.php">Новости</a></li>
			
			<li><a class="list__item" id="desk-contack" href="../contact.php">Контакты</a></li>
		</ul>
	</nav>
	
   <div>
    <input class="hide" id="hd-1" type="checkbox">
        <label class="docs-btn" for="hd-1"><img src="../img/burger.svg" alt=""> </label>
        <div class="docs__list">
            <ul class="nav-site" id="burger-menu">
                <li><a class="list__item" id="mobail-index" href="../index.php">Главная</a></li>

                <li><a class="list__item" id="mobail-adout" href="../about.php">О нас</a></li>

                <li><a class="list__item" id="mobail-parther" href="../partner.php">Партнеры</a></li>

                <li><a class="list__item" id="mobail-prace" href="../service.php">Услуги</a></li>

                <li><a class="list__item" id="mobail-docs" href="../docs.php">Документы</a></li>

                <li><a class="list__item" id="mobail-news" href="../news.php">Новости</a></li>

                <li><a class="list__item" id="mobail-contack" href="../contact.php">Контакты</a></li>
            </ul>	
        </div>
    </div>

	
</header>

    