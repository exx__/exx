<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap&subset=cyrillic" rel="stylesheet">
<link rel="stylesheet" href="../css/style.css">
<script src="../js/main.js"></script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/admin.js"></script>
<script src="js/ajax.js"></script>
<script src="../js/ajax_for_red.js"></script>
