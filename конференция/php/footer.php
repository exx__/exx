<footer class="footer">
	<img class="footer__logo" src="../img/logo.svg" alt="">
	
	<a target="_blank" href="https://vk.com/"><img class="footer__vk" src="../img/vk-social-network-logo_(4).svg" alt=""></a>
	
	<p class="footer__title">Сообщество Молодежного бизнес-инкубатора ЛГТУ.</p>
</footer>