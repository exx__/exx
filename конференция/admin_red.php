<?php session_start();
 if(!isset($_SESSION['login'])) {

header('Location: /admin');
}
?>
<!DOCTYPE html> <!-- Объявление формата документа -->

<html>
<head> <!-- Техническая информация о документе -->
<meta charset="UTF-8"> <!-- Определяем кодировку символов документа -->
<title>Благотворительный фонд</title> <!-- Задаем заголовок документа -->
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no"/>

<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="By PaulZi [2017-10-05]">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="filestyler-master/docs/example.css">
<link rel="stylesheet" href="filestyler-master/dist/filestyler.min.css">
<link rel="stylesheet" href="filestyler-master/dist/filestyler-theme.min.css">
<link rel="stylesheet" href="css/admin.css"> <!-- Подключаем внешнюю таблицу стилей -->
<link rel="stylesheet" href="css/style.css">
<script src="js/jquery-3.3.1.min.js"></script>


<script type="text/javascript" src="js/script.js"></script>
<script src="js/ajax.js"></script>
<script src="js/admin_red.js"></script>
<!-- <script src="script.js"></script> <!-- Подключаем сценарии -->
</head>
<body> <!-- Основная часть документа -->
<input type="submit" class="replace" name="add" onclick="replace_menu()" value="">
  <?php if($_SESSION['metod']=='news')
  {
    echo '  <h2>Новости</h2>';
  }
  if($_SESSION['metod']=='doc')
  {
    echo '  <h2>Документы</h2>';
  }
  if($_SESSION['metod']=='partner')
  {
    echo '  <h2>Партнеры</h2>';
  }
  if($_SESSION['metod']=='service')
  {
    echo '  <h2>Услуги</h2>';
  }
  ?>
          <form  class="settings__form" method="post" enctype="multipart/form-data">
              <?php
              if($_SESSION['metod']=='news')
              {
               echo '  <section class="sample">
                     <p>Файл</p>
                     <div class="filestyler filestyler_uninitialized filestyler_empty filestyler_image" data-filestyler=\'{"mode":"default"}\'>
                     <div class="filestyler__drop">
                         <div class="filestyler__list">

                             <button type="button" class="filestyler__clear"></button>
                             <label class="filestyler__file filestyler__plus">
                                 <input type="file" name="file" id="file" class="filestyler__input" accept=".jpg,.jpeg,.png,.gif,.svg">
                             </label>
                         </div>

                     </div>
                 </div>


               <div class="settings">
                 <p>Название новости</p>
               <input type="text" name="name" id="name" value="">
               <p>Текст новости</p>
               <textarea name="text" id="text" rows="8" cols="80"></textarea>
                <p>Выберите дату: </p><input type="date" id="date" name="calendar">
                    </div>
               </section>';
              }
              if($_SESSION['metod']=='doc')
              {
               echo '  <section class="sample">
                     <p>Файл</p>
                   <div class="filestyler filestyler_uninitialized filestyler_empty filestyler_image" data-filestyler=\'{"mode":"default"}\'>
                   <div class="filestyler__drop">
                       <div class="filestyler__list">

                           <button type="button" class="filestyler__clear"></button>
                           <label class="filestyler__file filestyler__plus">
                               <input type="file" name="file" id="file" class="filestyler__input" >
                           </label>
                       </div>

                   </div>
               </div>
               <div class="settings">
                 <p>Название документа</p>
               <input type="text" name="name" id="name" value="">
                    </div>
               </section>';
              }
              if($_SESSION['metod']=='partner')
              {
               echo '  <section class="sample">
                     <p>Файл</p>
                   <div class="filestyler filestyler_uninitialized filestyler_empty filestyler_image" data-filestyler=\'{"mode":"default"}\'>
                   <div class="filestyler__drop">
                       <div class="filestyler__list">

                           <button type="button" class="filestyler__clear"></button>
                           <label class="filestyler__file filestyler__plus">
                               <input type="file" name="file" id="file" class="filestyler__input" accept=".jpg,.jpeg,.png,.gif,.svg">
                           </label>
                       </div>

                   </div>
               </div>
               <div class="settings">
                 <p>Ссылка</p>
               <input type="text" name="link" id="link" required value="">
               </section>';
              }
              if($_SESSION['metod']=='service')
              {
               echo '  <section class="sample">
               <div class="settings">
                 <p>Название услуга</p>
               <input type="text" name="name" id="name" value="">
               <p>Описание услуги</p>
               <textarea name="text" id="text" rows="8" cols="80"></textarea>
                    </div>
                     </section>';
              }
               ?>

          </form>
          <?php
if($_SESSION['id']=='nan')
{
echo  '<input class="button_add" type="submit" name="add" value="Сохранить">';
}
else {
echo  '<input class="button_red" type="submit" name="red" value="Редактировать">';
}
?>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="filestyler-master/dist/jquery-iframe-ajax.min.js"></script>
<script src="filestyler-master/dist/filestyler.min.js"></script>
</body>
</html>
