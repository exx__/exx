<?php session_start();
 if(!isset($_SESSION['login'])) {

header('Location: /admin');
  }
?>
<!DOCTYPE html> <!-- Объявление формата документа -->
<html>
<head> <!-- Техническая информация о документе -->
<meta charset="UTF-8"> <!-- Определяем кодировку символов документа -->
<title>Сайт Конференции</title> <!-- Задаем заголовок документа -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/admin.css"> <!-- Подключаем внешнюю таблицу стилей -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap&subset=cyrillic" rel="stylesheet">
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/admin.js"></script>
<script src="js/ajax.js"></script>
<!-- <script src="js/ajax_for_red.js"></script> -->
<!-- <script src="script.js"></script> <!-- Подключаем сценарии -->

</head>
<body> <!-- Основная часть документа -->
<input type="submit" class="replace" name="add" onclick="replace()" value="">
<section class="information">


  <div class="information__partner">
    <div>Список  партнеров <input  class="input_add" type="submit" name="add" onclick="red('nan','partner')" value="Добавить"></div>

  </div>

  <div class="information__service">
    <div>
     Список услуг <input type="submit" class="input_add" name="add" onclick="red('nan','service')" value="Добавить"></div>

  </div>

  <div class="information__news">
    <div> Список новостей <input type="submit" class="input_add" name="add" onclick="red('nan','news')" value="Добавить"></div>

  </div>

  <div class="information__doc">
    <div> Список документов <input type="submit" class="input_add" name="add" onclick="red('nan','doc')" value="Добавить"></div>

  </div>
</section>

</body>
</html>
