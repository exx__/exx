<!DOCTYPE html> <!-- Объявление формата документа -->
<html>
<head> <!-- Техническая информация о документе -->
<meta charset="UTF-8"> <!-- Определяем кодировку символов документа -->
<title>Благотворительный фонд</title> <!-- Задаем заголовок документа -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/styleadmin.css"> <!-- Подключаем внешнюю таблицу стилей -->
<link rel="stylesheet" href="fonts/Panton/stylesheet.css">
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/add.js"></script>
<script src="js/ajax.js"></script>
<!-- <script src="script.js"></script> <!-- Подключаем сценарии -->

</head>
<body> <!-- Основная часть документа -->
<input type="submit" class="replace" name="add" onclick="replace()" value="Назад">
  <section class="news">
    <div> Список новостей <input type="submit" name="add" onclick="red('nan','news')" value="Добавить"></div>
  </section>
  <section class="children">
    <div>
     Список Детей <input type="submit" name="add" onclick="red('nan','children')" value="Добавить"></div>
  </section>
  <section class="team">
    <div> Список команд <input type="submit" name="add" onclick="red('nan','team')" value="Добавить"></div>
  </section>
  <section class="partners">
    <div> Список партнеров <input type="submit" name="add" onclick="red('nan','partner')" value="Добавить"></div>
  </section>
  <section class="report">
    <div> Список отчетов <input type="submit" name="add" onclick="red('nan','report')" value="Добавить"></div>
  </section>
  <section class="report_doc">
    <div> Список документов <input type="submit" name="add" onclick="red('nan','report_doc')" value="Добавить"></div>
  </section>
</body>
</html>
