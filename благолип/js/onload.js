window.onload = function (){
	
	
	var news = document.getElementById("news");
	
	var helpSum = document.getElementById("help-sum");
	
	var helpChild = document.getElementById("help-child");
	
	var report= document.getElementById("report");
	
	var partner = document.getElementById("partner");
	
	var team = document.getElementById("team");
	
	var navProgram = document.getElementById("nav-program");
	
	var navNeedHelp = document.getElementById("nav-need-help");
	
	var navChildren = document.getElementById("nav-children");
	
	var navPartner = document.getElementById("nav-partner");
	
	var navTeam = document.getElementById("nav-team");
	
	var navReport = document.getElementById("nav-report");
	
	if ( document.getElementById("programm") != null){
		navProgram.style.color = "#43bfed";
	};
	
	if ( document.getElementById("help") != null){
		navNeedHelp.style.color = "#43bfed";
	};
	
	
	
	if (news != null){
		
		$.ajax({
			async: false,
			url: '../php/read.php',
			type: "POST",
			data: "metod=read-news",
			success: function(news){
			  //alert(news);
			  news_arr=news;
			}
		  });
		  arrNews=JSON.parse(news_arr);

		$('.news__left').html('<img class="news__left__img" src="'+arrNews[arrNews.length-1][3]+'" alt=""><p class="news__left__title">'+arrNews[arrNews.length-1][2]+'</p><p class="news__left__info">'+arrNews[arrNews.length-1][4]+'</p><a href="'+arrNews[arrNews.length-1][0]+'">'+arrNews[arrNews.length-1][0]+'</a>');
		

		for	(var i = arrNews.length - 5  ; i < arrNews.length-1; i++){
		$('.news__right').append('<a href="'+arrNews[i][0]+'" class="news__right__block"><img class="news__right__img"      src="'+arrNews[i][3]+'" alt=""><p   class="news__right__title">'+arrNews[i][2]+'</p><p  class="news__right__info">'+arrNews[i][4]+'</p></a>');

	};
		
	};
	
	
	
	
	
	
	
	
	if (helpSum != null){
		
		  $.ajax({
			async: false,
			url: '../php/read.php',
			type: "POST",
			data: "metod=read-children",
			success: function(helpSum){
			  //alert(helpSum);
			  helpSum_arr= helpSum;
			}
		  });
		  arrhelpSum = JSON.parse(helpSum_arr);
			
		
		for	(var i = 0; i < arrhelpSum.length ; i++){
		$('#help-sum').append('<div class="main-help__block-children"><img class="block-children__img" src="'+ arrhelpSum[i][8]+'" alt=""><p class="block-children__name">'+ arrhelpSum[i][0]+'</p><p class="block-children__diagnosis">'+ arrhelpSum[i][4]+'</p><p class="block-children__info">'+ arrhelpSum[i][5]+'</p><a class="block-children__link" href="#">Подробнее</a></div>');
		};
	};
	
	
	
	
	
	
	
	if (helpChild != null){
		navChildren.style.color = "#43bfed";
		$.ajax({
			async: false,
			url: '../php/read.php',
			type: "POST",
			data: "metod=read-children",
			success: function(helpChild){
			  //alert(helpChild);
			  helpChild_arr= helpChild;
			}
		  });
		  arrhelpChild = JSON.parse(helpChild_arr);
		
		for	(var i = 0; i < arrhelpChild.length ; i++){
		$('#help-child').append('<div class="main-help__block-children"><img class="block-children__img" src="'+arrhelpChild[i][8]+'" alt=""><p class="block-children__name">'+arrhelpChild[i][3]+'</p><p class="block-children__diagnosis">Диагноз:</p><p class="block-children__info">'+arrhelpChild[i][5]+'</p><a class="block-children__link" href="#">Подробнее</a><p class="block-children__need">Требуется собрать:</p><p class="block-children__sum">'+arrhelpChild[i][6]+'</p></div>');
		};
	};
	
	
	
	
	
	
	
	if (report != null){
		navReport.style.color = "#43bfed";
		
		$.ajax({
			async: false,
			url: '../php/read.php',
			type: "POST",
			data: "metod=read-report",
			success: function(msq){
			  //alert(msq);
			  result_arr=msq;
			}
		  });
		  var report=JSON.parse(result_arr);
		
		
		
		
		  $.ajax({
			async: false,
			url: '../php/read.php',
			type: "POST",
			data: "metod=read-report_doc",
			success: function(report){
			  
			  report_arr = report;
			}
		  });
		  var report_doc=JSON.parse(report_arr);
		
	
		
		for	(var i = 0; i < report.length ; i++){
			$('#report').append('<p class="report" >'+report[i][0]+'</p><hr>');
		};
		

		
		
		for	(var i = 0; i < report_doc.length ; i++){
			
			$('#report-docs').append('<a href="'+report_doc[i][2]+'" class="list__detail">'+report_doc[i][0]+'<img class="detail__img" src="img/download.svg" alt=""></a>');
		};
	};
	
	
	
	
	
	
	
	
	
	
	if (partner != null){
		navPartner.style.color = "#43bfed";
//		не работает
		  $.ajax({
			async: false,
			url: '../php/read.php',
			type: "POST",
			data: "metod=read-partners",
			success: function(partners){
			  //alert(partners);
			  partners_arr=partners;
			}
		  });
   		
		var arrPartner=JSON.parse(partners_arr);
//		var arrPartner = [["АРТ- Мебель","../img/part1.jpg"],["АРТ- Мебель","../img/part2.png"],["АРТ- Мебель","../img/part3.png"]];
		
		for	(var i = 0; i < arrPartner.length ; i++){
			$('#partner').append('<p class="partner__title">'+arrPartner[i][0]+'</p><img class="partner__img" src="'+arrPartner[i][2]+'" alt="">');
		};
	};
	
	
	
	
	
	
	
	
	
	if (team != null){
		navTeam.style.color = "#43bfed";
		  $.ajax({
			async: false,
			url: '../php/read.php',
			type: "POST",
			data: "metod=read-team",
			success: function(team){
			  //alert(team);
			  team_arr = team;
			}
		  });
  		var arrTeam =JSON.parse(team_arr);
		
//		var arrTeam = [["img/team1.png","Юлия Имамкулиева","Президент БФ «Особенные дети».Мама четверых детей."],["img/team1.png","Юлия Имамкулиева","Президент БФ «Особенные дети».Мама четверых детей."],["img/team1.png","Юлия Имамкулиева","Президент БФ «Особенные дети».Мама четверых детей."],["img/team1.png","Юлия Имамкулиева","Президент БФ «Особенные дети».Мама четверых детей."],["img/team1.png","Юлия Имамкулиева","Президент БФ «Особенные дети».Мама четверых детей."]];
		
		
		
		for	(var i = 0; i < arrTeam.length ; i++){
			$('#team').append('<div class="teams__personal"><img class="personal__img" src="'+arrTeam[i][3]+'" alt=""><p class="personal__title">'+arrTeam[i][0]+'</p><p class="personal__text">'+arrTeam[i][2]+'</p></div>');
		};
	};
	
	
	
	
	
	
	
}