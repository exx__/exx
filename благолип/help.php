<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<title>фонд</title>
	<?php
         	require ('php/head.php');
    ?>
</head>

<body>



	<?php
  require ('php/nav.php');
?>
	<h2 class="title__main">
		<img class="title__main-star" src="img/Star.svg" alt="">
		Получить помощь
		<img class="title__main-star" src="img/Star.svg" alt="">
	</h2>
<div class="bg-main-child">	
	<section class="children__main" id="help">
		<p class="children__main-text">Благотворительный фонд «Особенные дети» - единственный фонд в России который занимается проблемами детей с Центральной Нервной Системой и Опорно Двигательного Аппарата неясного генеза.<br> Фон оказывает всестороннюю поддержку детям до 21 года на всей территории РФ. 
		</p>
		
		<p class="children__main-text">Для первичного обращения в фонд, вам необходимо предоставить пакет документов для предварительного рассмотрения:
		</p>
		
		<ul class="children__main-ul">
			<li class="children__main-ul__li">Заявление-анкета (в анкете должны быть заполнены все поля, подписать от руки и прислать в сканированном виде).</li>
			<li class="children__main-ul__li">Свежие выписки, подтверждающие диагноз и необходимость лечения / реабилитации / лекарства/ оборудования.</li>
			<li class="children__main-ul__li">Счет из клиники, где будет проходить лечение / реабилитация или покупка лекарств / оборудование.</li>
			<li class="children__main-ul__li">Свидетельство о рождении, справка об инвалидности.</li>
			<li class="children__main-ul__li">Паспорт одного родителя (разворот с фотографией и паспортными данными)</li>
			<li class="children__main-ul__li">Фотографии в хорошем качестве (семейные, портретные с ребенком)</li>
		</ul>
		
		<p class="children__main-text smal-indent">Документы нужно отправить на адрес электронной почты </p>
		
		<a href="mail:osobennyedeti48@mail.ru" class="children__main-mail">osobennyedeti48@mail.ru</a>
		<p class="children__main__text"><span class="main__text__red">Важно</span>: Все документы должны хорошо читаться, быть четкими не обрезанными.</p>
		<p class="children__main__text">После получения мы рассмотрим ваши документы. Если ваша заявка будет одобрена, мы вышлем вам дополнительный список документов, необходимый для принятия окончательного решения.</p>
	</section>

</div>


	<h2 class="title__main">
		<img class="title__main-star" src="img/Star.svg" alt="">
		Волонтерство
		<img class="title__main-star" src="img/Star.svg" alt="">
	</h2>
	
		<section class="children__main">
		<p class="children__main-text">Волонтёр — человек, занимающийся общественной полезной деятельностью на безвозмездной основе. Работа любого Благотворительного фонда не возможна без помощи волонтеров. - Хотите стать частью нашей большой дружной команды? 
– C радостью будем ждать Вас, <a class="link" href="volunteer.php">Заполняйте анкету</a>  мы обязательно с Вами свяжемся.
Почта для получения анкет osobennyedeti48@mail.ru
		</p>
		
		
	</section>




	<h2 class="title__main">
		<img class="title__main-star" src="img/Star.svg" alt="">
		Сделать пожертвование
		<img class="title__main-star" src="img/Star.svg" alt="">
	</h2>

	<section class="help-sms">
		<p class="help-sms__title">СМС пожертвование</p>
		<p class="help-sms__text">Для того чтобы помочь фонду вы можете отправить СМС на короткий номер</p>
		<p class="help-sms__title-red">4444</p>
		<p class="help-sms__text">с текстом</p>
		<p class="help-sms__title-red">ПЕЛЬМЕШКА 100</p>
		<p class="help-sms__text">и с вашего счета будет списано 100 рублей (вы можете указать любую сумму).</p>
		<p class="help-sms__info">Для абонентов следующих операторов комиссия за отправку СМС с абонентов составляет 0%:</p>

		<div class="help-sms__operator">
			<img src="img/beeline.svg" alt="">
			<img src="img/mega.svg" alt="">
			<img src="img/mts.svg" alt="">
			<img src="img/tele2.svg" alt="">
		</div>

		<a class="help-sms__detail-link" href="#">Подробности оказания услуги и лимиты операторов</a>
	</section>

	<h2 class="title__main">
		<img class="title__main-star" src="img/Star.svg" alt="">
		Перевод на счет
		<img class="title__main-star" src="img/Star.svg" alt="">
	</h2>

	<section class="account">
		<p class="account__info">Для поддержки нашего фонда вы можете выполнить перевод на наш счет в Сбербанке. Для этого используйте реквизиты, указанные ниже. Не забудьте отметить назначение платежа - Благотворительное пожертвование</p>

		<p class="account__adress">
			БФ ‘’Особенные дети”<br>
			Тел. 89046899928<br>
			ИНН/КПП 4823076517/ 482301001<br>
			ОГРН 1184827010136 <br>
			Юридический адрес: 39890,<br>
			Липецкая область, г. Липецк,<br>
			ул. Физкультурная, д. 7, к. 31.<br>
			P/C 4070 3810935000001041<br>
			Филиал ПАО «Сбербанк» г. Липецк<br>
			К/С 30101810800000000604<br>
			БИК 044206604<br>
		</p>



		<h2 class="title__main">
			<img class="title__main-star" src="img/Star.svg" alt="">
			Корпоративные партнеры
			<img class="title__main-star" src="img/Star.svg" alt="">
		</h2>


		<p class="account__info">Корпоративные партнеры (ссылку Для Юридических лиц) могут сделать благотворительное пожертвование двумя способами: перевести деньги на счет фонда(ссылку счет фонда) или заключить с фондом договор пожертвования.</p>
	</section>

	<section class="adress">
		<img class="adress__img" src="img/bear.svg" alt="">
		<div class="adress__right-block">
			<h2 class="title__main-help">
				<img class="title__main-star" src="img/Star.svg" alt="">
				Адреса ящиков для пожертвований
				<img class="title__main-star" src="img/Star.svg" alt="">
			</h2>

			<ul class="right-block__list">
				<li class="children__main-ul__li"> г. Липецк, ул. Моршанская, д. 8, магазин "МИР"</li>
				<li class="children__main-ul__li">г. Липецк, ул. Стаханова 12/1, магазин "Разливные Напитки"</li>
				<li class="children__main-ul__li">г. Липецк, ул. Катукова 51,трц «Ривьера», дом текстиля "Togas"</li>
			</ul>


		</div>
	</section>

	<?php
     require ('php/footer.php');
?>



	<script src="js/main.js"></script>
</body>

</html>