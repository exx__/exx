<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<title>фонд</title>
	<?php
         	require ('php/head.php');
    ?>
</head>

<body>



<?php
  require ('php/nav.php');
?>
	<h2 class="title__main">
		<img class="title__main-star" src="img/Star.svg" alt="">
		Получить помощь
		<img class="title__main-star" src="img/Star.svg" alt="">
	</h2>
<div class="bg-main-child">	
	<section class="children__main">
		<p class="children__main-text">Благотворительный фонд «Особенные дети» - единственный фонд в России который занимается проблемами детей с Центральной Нервной Системой и Опорно Двигательного Аппарата неясного генеза.<br> Фон оказывает всестороннюю поддержку детям до 21 года на всей территории РФ. 
		</p>
		
		<p class="children__main-text">Для первичного обращения в фонд, вам необходимо предоставить пакет документов для предварительного рассмотрения:
		</p>
		
		<ul class="children__main-ul">
			<li class="children__main-ul__li">Заявление-анкета (в анкете должны быть заполнены все поля, подписать от руки и прислать в сканированном виде).</li>
			<li class="children__main-ul__li">Свежие выписки, подтверждающие диагноз и необходимость лечения / реабилитации / лекарства/ оборудования.</li>
			<li class="children__main-ul__li">Счет из клиники, где будет проходить лечение / реабилитация или покупка лекарств / оборудование.</li>
			<li class="children__main-ul__li">Свидетельство о рождении, справка об инвалидности.</li>
			<li class="children__main-ul__li">Паспорт одного родителя (разворот с фотографией и паспортными данными)</li>
			<li class="children__main-ul__li">Фотографии в хорошем качестве (семейные, портретные с ребенком)</li>
		</ul>
		
		<p class="children__main-text smal-indent">Документы нужно отправить на адрес электронной почты </p>
		
		<a href="mail:osobennyedeti48@mail.ru" class="children__main-mail">osobennyedeti48@mail.ru</a>
		<p class="children__main__text"><span class="main__text__red">Важно</span>: Все документы должны хорошо читаться, быть четкими не обрезанными.</p>
		<p class="children__main__text">После получения мы рассмотрим ваши документы. Если ваша заявка будет одобрена, мы вышлем вам дополнительный список документов, необходимый для принятия окончательного решения.</p>
		
		
		
		
	</section>

</div>
	

<?php
     require ('php/footer.php');
?>



	<script src="js/main.js"></script>
</body>

</html>