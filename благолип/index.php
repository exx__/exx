<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<title>фонд</title>
	<?php
         	require ('php/head.php');
    ?>

</head>

<body>



	<?php
         	require ('php/nav.php');
     	?>


	<section class="info">
		<div class="info__text">
		
			<p class="info__text-p">Коллектив фонда был сформирован с целью оказания бескорыстной общественно - полезной работы. Наши специалисты прилагают максимальные усилия для решения проблем детей с ЦНС и Опорно-двигательного аппарата неясного генеза.</p>
			<p class="info__text-p">В арсенале фонда более 10 программ для поддержки и оказания помощи родителям и детям, оказавшимся в столь непростых условиях.Не откладывайте добрые дела на завтра, присоединяйтесь!Вместе мы сможем больше!</p>
			<p class="info__text-blue">Не откладывайте добрые дела на завтра, присоединяйтесь! Вместе мы сможем больше!</p>
		</div>
		<img class="info__img" src="img/child-1.png" alt="">
	</section>

	<section class="help">
		<img class="help__img" src="img/girl.png" alt="">
		<div class="help__text">

			<p class="help__text-blue">Чем я могу помочь?</p>

			<p class="help__text-simpl">Для того чтобы помочь фонду вы можете отправить СМС на короткий номер</p>

			<p class="help__text-blue">44444</p>

			<p class="help__text-simpl">с текстом</p>

			<p class="help__text-blue">помогу 100</p>

			<p class="help__text-simpl">и с вашего счета будет списано 100 рублей (вы можете указать любую сумму).</p>

			<a class="help__text__link" href="#">Подробности оказания услуги</a>

			<p class="help__text-info">Вы так же можете перевести на наш счет Сбербанка:</p>

			<div class="help__text__blocs-info">
				<div class="help__text-blocs-info__text">
					БФ ‘’Особенные дети”<br>
					Тел. 89046899928<br>
					ИНН/КПП 4823076517/ 482301001<br>
					ОГРН 1184827010136<br>
					Юридический адрес: 39890,<br>
					Липецкая область, г. Липец,<br>
					ул. Физкультурная, д. 7, к. 31
				</div>

				<div class="help__text-blocs-info__text">
					P/C 4070 3810935000001041<br>
					Филиал ПАО «Сбербанк» г. Липецк<br>
					К/С 30101810800000000604<br>
					БИК 044206604<br>
					Назначение платежа:<br>
					«Благотворительное пожертвование»
				</div>
			</div>

		</div>
	</section>


	<section class="phone-info">
		<p class="phone-info__title">Отправьте СМС на номер 3443 со словом «ПРЕФИКС», указав через пробел сумму пожертвования</p>

		<p class="phone-info__ul-title">Требования для абонентов Мегафон:</p>

		<ul class="phone-info__list">
			<li class="ul__item">Минимальная сумма одного Платежа — 1 руб.;</li>
			<li class="ul__item">Максимальная сумма единовременного Платежа — 15 000 руб.;</li>
			<li class="ul__item">Максимальная сумма Платежей в сутки — 40 000 руб.;</li>
			<li class="ul__item">Максимальная сумма Платежей в месяц — 40 000 руб.</li>

		</ul>



		<p class="phone-info__ul-title">Требования для абонентов МТС:</p>

		<ul class="phone-info__list">
			<li class="ul__item">Минимальная сумма одного Платежа — 10 руб.</li>
			<li class="ul__item">С каждого успешного платежа МТС взимает с абонента комиссию в размере 10 рублей (в том числе НДС)</li>
			<li class="ul__item">Максимальная сумма платежа: 1 000 — для услуг мобильной связи и Yota 5 000 — для остальных услуг и категорий</li>
			<li class="ul__item">Максимальная сумма платежей в сутки: 5000 руб. или не более 40 000 руб. в месяц.</li>

		</ul>


		<p class="phone-info__ul-title">Требования для абонентов Билайн:</p>

		<ul class="phone-info__list">
			<li class="ul__item">Минимальная сумма одного Платежа — 10</li>
			<li class="ul__item">Максимальная сумма одного Платежа — 5 000</li>
			<li class="ul__item">Максимальная сумма платежей за день — 15 000</li>
			<li class="ul__item">Максимальная сумма платежей в неделю — 40 000</li>
			<li class="ul__item">Максимальная сумма платежей в месяц 40’000 руб., максимум 50 транзакций</li>
		</ul>



		<p class="phone-info__ul-title">Требования для абонентов TELE2:</p>

		<ul class="phone-info__list">
			<li class="ul__item">Минимальная сумма одного Платежа — 10 руб.</li>
			<li class="ul__item">Максимальная сумма платежа: 1 000 — для услуг мобильной связи и Yota 5 000 — для остальных услуг и категорий</li>
			<li class="ul__item">Максимальное число платежей в сутки: 10 или не более 50 месяц.</li>

		</ul>
	</section>

	<section class="news" id="news">
		<!--начало секции новостей-->

		<a href="#" class="news__left">
			<!--начало большой новости-->

		</a><!-- конец большой новости-->

		<div class="news__right">
			<!-- блок с маленькими новостями-->


		</div><!-- конец блока с маленькими новостями-->

	</section>
	<!--end секции новостей-->

	<section class="join">
		<img class="join__img" src="img/bg-four.png" alt="">
		<p class="join__text">Присоединятесь!<br> Поможем детям вместе!</p>
	</section>


	<?php
        require ('php/footer.php');
    ?>

</body>

</html>