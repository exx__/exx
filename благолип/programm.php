<!DOCTYPE html> <!-- Объявление формата документа -->
<html>
<head> <!-- Техническая информация о документе -->
<meta charset="UTF-8"> <!-- Определяем кодировку символов документа -->
	<?php
         	require ('php/head.php');
    ?>
<!-- <script src="script.js"></script> <!-- Подключаем сценарии -->
</head>
<body> <!-- Основная часть документа -->
	
	<?php
         require ('php/nav.php');
     ?>
     	
  <section class="programm" id="programm">
   
    <h2>
      <img class="title__main-star"  src="img/star.png" alt="">
      ПРОГРАММЫ ФОНДА
      <img class="title__main-star"  src="img/star.png" alt="">
    </h2>
    
    <ul class="programm__ul">
      <li>
        <img src="img/doctor.png" alt="">
          <p class="programm__ul-name">Поиск доктора и больницы</p>
            <p class="programm__ul-text">Подбор необходимого специалиста и клиники которые смогут провести необходимую диагностику.</p>
      </li>
      <li>
          <img src="img/pig.png" alt="">
          <p class="programm__ul-name">Финансовая помощь</p>
            <p class="programm__ul-text">Приобретение медикаментов и медицинской техники.</p>
      </li>
      <li>
          <img src="img/walk.png" alt="">
          <p class="programm__ul-name">Адаптивная физическая нагрузка</p>
            <p class="programm__ul-text">Спортивно-оздоровительного характера, направленная на реабилитацию и адаптацию детей.</p>
        </li>
      <li>
          <img src="img/support.png" alt="">
          <p class="programm__ul-name">Ваш психолог</p>
            <p class="programm__ul-text">Помощь детям и родителям в психологическом преодолении проблем.</p>
      </li>
  </ul>
  </section>
  <section class="heartCity">
        <p class="heartCity__name"> Благотворительная лавка<br>“Сердце города”</p>
      <p class="heartCity__text"> Мы принимаем изделия ручной работы:
      украшения, открытки, блокноты, игрушки, топиарии, вязанные изделия,
      сладости и т.д.,
      Вырученные средства направляются на оказание помощи детям и реализацию благотворительных программ фонда. </p>
  </section>
  <section class="programm">
    <ul class="programm__ul">
      <li>
        <img src="img/friendship.png" alt="">
          <p class="programm__ul-name">Правовая грамотность</p>
            <p class="programm__ul-text">Разъяснения прав возможностей родителей и детей, помощь в подготовке необходимой документации.</p>
      </li>
      <li>
          <img src="img/honeymoon.png" alt="">
          <p class="programm__ul-name">Добрый рейс</p>
            <p class="programm__ul-text">Поддержка детей в больницах:Мастер классы,Больничные клоуны,День Именинника! </p>
      </li>
  </ul>
  </section>
  <section class="heartBox">
        <p class="heartBox__name">Коробочка смелости</p>
        <p class="heartBox__text"> Во время лечения детям приходится терпеть массу очень неприятных и болезненных процедур.
В отделении в процедурном кабинете всегда стоит «Коробка смелости», из которой дети извлекают маленькие сюрпризы в награду за проявленную смелость в боях за собственное здоровье!</p>
  </section>
  
	<?php
        require ('php/footer.php');
    ?>
</body>
</html>
