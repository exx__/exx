	<header class="header">
		<a class="header__logo-fond" href="../index.php"><img src="img/logo-fond.png" alt="" class="logo-fond"></a>
		<h1 class="title-fond">Благотворительный фонд помощи детям</h1>
		
		<div class="header__block-messeng">
			<a tel="8 800 999 777 22 11" class="header__block-messeng__tel">8 800 999 777 22 11</a>
			<a href="#" class="header__block-messeng__icons"><img src="img/img-mes-1.png" alt=""></a>
			<a href="#" class="header__block-messeng__icons"><img src="img/img-mes-2.png" alt=""></a>
			<a href="#" class="header__block-messeng__icons"><img src="img/img-mes-3.png" alt=""></a>
			
		</div>
		
			<nav class="nav">
				<ul class="nav-site">
					<li><a href="../programm.php" id="nav-program">Программа фонда</a></li>
					<li><a href="#" class="nav-site__palka">|</a></li>
					<li><a href="../help.php" id="nav-need-help">Чем помочь</a></li>
					<li><a href="#"  class="nav-site__palka">|</a></li>
					<li><a href="../help-child.php" id="nav-children">Дети</a></li>
					<li><a href="#"  class="nav-site__palka">|</a></li>
					<li><a href="../partner.php" id="nav-partner">Партнеры</a></li>
					<li><a href="#"  class="nav-site__palka">|</a></li>
					<li><a href="../team.php" id="nav-team">Наша Команда</a></li>
					<li><a href="#"  class="nav-site__palka">|</a></li>
					<li><a href="../report.php" id="nav-report">Отчеты</a></li>
				</ul>
			</nav>

		<input class="hide" id="hd-1" type="checkbox">
		<label class="docs-btn" for="hd-1"><img src="../img/burger.svg" alt=""> </label>
		
		<div class="docs__list">
			<ul class="nav-site">
					<li><a href="../programm.php" id="nav-program">Программа фонда</a></li>
					<li><a href="../help.php" id="nav-need-help">Чем помочь</a></li>
					<li><a href="../help-child.php" id="nav-children">Дети</a></li>
					<li><a href="../partner.php" id="nav-partner">Партнеры</a></li>
					<li><a href="../team.php" id="nav-team">Наша Команда</a></li>
					<li><a href="../report.php" id="nav-report">Отчеты</a></li>
				</ul>	
		</div>

	</header>