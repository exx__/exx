<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<title>фонд</title>
	<?php
         	require ('php/head.php');
    ?>
</head>

<body>



<?php
  require ('php/nav.php');
?>
	<div class="bg-main-child">	
	<h2 class="title__main">
		<img class="title__main-star" src="img/Star.svg" alt="">
		Волонтерство
		<img class="title__main-star" src="img/Star.svg" alt="">
	</h2>
	
	<section class="volunter">
		<p class="volunter__description">Волонтёр — человек, занимающийся общественной полезной деятельностью на безвозмездной основе.<br>
Работа любого Благотворительного фонда не возможна без помощи волонтеров.<br>
- Хотите стать частью нашей большой дружной команды? <br>
– C радостью будем ждать Вас!<br>
Заполняйте анкету мы обязательно с Вами свяжемся.</p>
	<h3 class="volunter__title-form">Анкета волонтера</h3>
	<form action="" class="volunter__form">
		
		<div class="form__left-bar">
			
			<input type="text" class="left-bar__input" placeholder="ФИО" required>
			
			<input type="phone" class="left-bar__input" placeholder="Телефон" required>
			
			<input type="mail" class="left-bar__input" placeholder="E-mail" required>
			
			<input type="text" class="left-bar__input" placeholder="Дата рождения" required>
			
			<input type="text" class="left-bar__input" placeholder="Соц. сети" required>
			
			
			<div class="left-bar__radio">		
				  <input class="radio__input" type="radio" name="gender" id="radio1" value="female">
				  <label class="radio__label" for="radio1">Женат\Замужем</label>
				  <input  class="radio__input" type="radio" id="radio2" name="gender" value="other">  
				  <label class="radio__label" for="radio2">Холост\Не замужем</label>
			</div>
			
			
			
		</div>
			
		<div class="form__right-bar">
			<textarea required class="right-bar__textarea" name="" id="" placeholder="Интересы, хобби, увлечения" ></textarea>
			
			<textarea required class="right-bar__textarea" name="" id="" placeholder="Имеется ли опыт работы волонтером? Какой?" ></textarea>
			
			<textarea required class="right-bar__textarea" name="" id="" placeholder="Чем вы можете помочь?" ></textarea>
		</div>
		
	</form>
	
	<input type="submit" class="left-bar__input-submit" value="Отправить">
	
	</section>
	</div>
	<section class="legal">
			<h3 class="title__main">
				<img class="title__main-star" src="img/Star.svg" alt="">
				Волонтерство
				<img class="title__main-star" src="img/Star.svg" alt="">
			</h3>
			
			<p class="legal__text">
				Приглашаем бизнес стать участникам корпоративной благотворительности. Корпоративная благотворительность — это добровольная деятельность коммерческой организации по оказанию помощи нуждающимся или по поддержке социальных проектов. Одной их форм благотворительности является корпоративное волонтерство (добровольчество) — добровольное участие сотрудников на безвозмездной основе в различных социальных программах при поддержке своей компании. Корпоративное волонтерство является элементом корпоративной социальной ответственности и стратегии устойчивого развития предприятия в целом. 
			</p>
			
			<p class="legal__title-ul">Участие компании в корпоративной благотворительности дает бизнесу значительные преимущества:</p>
			
			<ul class="legal__list">
				<li class="list__li">повышение престижа компании;</li>
				<li class="list__li">доверие и эмоциональная привязка потребителей;</li>
				<li class="list__li">дополнительная реклама и повышение узнаваемости;</li>
				<li class="list__li">рост стоимости компании за счет повышения оценки ее репутации;</li>
				<li class="list__li">облегчение доступа к инвестициям.</li>
			</ul>
			
			<p class="legal__title-ul">Вы можете проявить социальную активность и ответственность, выбрав подходящий для вас вариант сотрудничества с нашим фондом: </p>
			
			<ul class="legal__list">
				<li class="list__li">сделать благотворительное пожертвование, в т.ч. оказав адресную помощь конкретному ребенку; </li>
				<li class="list__li"><a href="#">стать спонсором одного из наших проектов</a></li>
				<li class="list__li">организовать благотворительную акцию с перечислением фиксированной суммы с товара или услуги;</li>
				<li class="list__li">установить в торговых точках ящики для сбора пожертвований; </li>
				<li class="list__li">предоставить помещение для организации мероприятий фонда (концертные залы, спортивные и развлекательные площадки, торговые площади);</li>
				<li class="list__li">принять участие в наших мероприятиях (ярмарки, концерты, спортивные мероприятия и т.д.);</li>
				<li class="list__li">помочь фонду в продвижении - изготовление и размещение рекламы, статьи и ссылки о фонде на ваших сайтах и в группах в социальных сетях.</li>
			</ul>
			
			<p class="legal__text">
				 Мы рады сотрудничеству с коммерческими и общественными организациями, мы стараемся сделать совместную работу наиболее комфортной и выгодной для обеих сторон! Информацию о наших партнерах мы размещаем на нашем сайте, в группах в социальных сетях, рассказываем о них в СМИ и наших информационных материалах. Становитесь социально ответственной организацией вместе с нами!
			</p>
			
			<p class="legal__text">
				Для обсуждения возможного варианта сотрудничества свяжитесь в нами по номеру телефона: <a class="legal__text__phone" tel="89046861220">89046861220</a>,
				<a class="legal__text__phone" tel="89046899928">89046899928</a>.
			</p>
	</section>
	

<?php
     require ('php/footer.php');
?>



	<script src="js/main.js"></script>
</body>

</html>