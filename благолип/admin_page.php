<?php session_start();
require 'php/conect.php'; ?>
<!DOCTYPE html> <!-- Объявление формата документа -->

<html>
<head> <!-- Техническая информация о документе -->
<meta charset="UTF-8"> <!-- Определяем кодировку символов документа -->
<title>Благотворительный фонд</title> <!-- Задаем заголовок документа -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/styleadmin.css"> <!-- Подключаем внешнюю таблицу стилей -->
<link rel="stylesheet" href="fonts/Panton/stylesheet.css">
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/ajax.js"></script>
<!-- <script src="script.js"></script> <!-- Подключаем сценарии -->
</head>
<body> <!-- Основная часть документа -->

  <section class="settings">
    <?php
    if($_SESSION['metod']=='news')
    {
      echo '<h1>
        Редактирование новости
      </h1>';
        }
    if($_SESSION['metod']=='team')
    {
      echo '<h1>
        Редактирование команды
      </h1>';
    }
    if($_SESSION['metod']=='children')
    {
      echo '<h1>
        Редактирование детей
      </h1>';
    }
    if($_SESSION['metod']=='partner')
    {
      echo '<h1>
        Редактирование партнеров
      </h1>';
    }

    if($_SESSION['metod']=='report')
    {

      echo '<h1>
        Редактирование отчетов
      </h1>';

    }
    if($_SESSION['metod']=='report_doc')
    {
      echo '<h1>
        Редактирование документов к отчетам
      </h1>';
    }
    ?>

  <!-- <form method="POST" action="php/add.php" enctype="multipart/form-data" class="settings__form"> -->
 <form method="POST" enctype="multipart/form-data" class="settings__form">
          <?php
          if($_SESSION['id']!='nan')
          {
            if($_SESSION['metod']=='news')
          {
            $get_news=pg_query($dbconn,"Select * from news where id_news=".$_SESSION['id']);
            $get_news=pg_fetch_assoc($get_news);
          echo '
              <div class="settings__form-group">
               <label for="file" class="col-sm-2 control-label">Файл  </label>
              <input id="sortpicture" type="file" class="settings__form-file" name="sortpic" />
              </div>
              <div class="settings__inf">
                <p> Название новости </p>
                  <input type="text" class="settings__inf-name" id="name" name="name" value="'.$get_news['name_news'].'">
                <p> Описание новости </p>
                  <textarea name="text" id="text" rows="8" >'.$get_news['text_news'].'</textarea>
                <label for="start">Дата события</label>
                  <input class="settings__inf-date" type="date" id="date" name="date" value="'.$get_news['date_news'].'">
              </div>
            ';

              }
              if($_SESSION['metod']=='team')
            {
              $get_team=pg_query($dbconn,"Select * from team where id_people=".$_SESSION['id']);
              $get_team=pg_fetch_assoc($get_team);
            echo '
            <div class="settings__form-group">
             <label for="file" class="col-sm-2 control-label">Файл  </label>
            <input id="sortpicture" type="file" class="settings__form-file" name="sortpic" />
            </div>
                <div class="settings__inf">
                  <p> Имя Сотрудника </p>
                    <input type="text" class="settings__inf-name" id="name" name="name" value="'.$get_team['name_people'].'">
                  <p> Описание сотрудника </p>
                    <textarea name="text" id="text" rows="8" >'.$get_team['text_people'].'</textarea>

                </div>
              ';

                }
                if($_SESSION['metod']=='partner')
              {
                $get_partner=pg_query($dbconn,"Select * from partners where id_partner=".$_SESSION['id']);
                $get_partner=pg_fetch_assoc($get_partner);
              echo '
                  <div class="settings__form-group">
                   <label for="file" class="col-sm-2 control-label">Файл  </label>
                  <input id="sortpicture" type="file" class="settings__form-file" name="sortpic" />
                  </div>
                  <div class="settings__inf">
                    <p> Название </p>
                      <input type="text" class="settings__inf-name" id="name" name="name" value="'.$get_partner['name_partner'].'">


                  </div>
                ';

                  }
                  if($_SESSION['metod']=='report')
                {
                  $get_partner=pg_query($dbconn,"Select * from report where id_report=".$_SESSION['id']);
                  $get_partner=pg_fetch_assoc($get_partner);
                echo '
                <div class="settings__form-group">
                <p> Описание </p>
                  <textarea name="text" id="text" rows="8" >'.$get_partner['text_report'].'</textarea>


                    </div>
                  ';

                    }
                    if($_SESSION['metod']=='report_doc')
                  {
                    $get_partner=pg_query($dbconn,"Select * from report_doc where id_report_doc=".$_SESSION['id']);
                    $get_partner=pg_fetch_assoc($get_partner);
                  echo '
                      <div class="settings__form-group">
                       <label for="file" class="col-sm-2 control-label">Файл  </label>
                      <input id="sortpicture" type="file" class="settings__form-file" name="sortpic" />
                      </div>
                      <div class="settings__inf">
                        <p> Название </p>
                          <input type="text" class="settings__inf-name" id="name" name="name" value="'.$get_partner['name_doc'].'">


                      </div>
                    ';

                      }
                      if($_SESSION['metod']=='children')
                    {
                      $get_partner=pg_query($dbconn,"Select * from children where id_children=".$_SESSION['id']);
                      $get_partner=pg_fetch_assoc($get_partner);
                      $get_type="SELECT text_type FROM type_children where id_type=".$get_partner['id_type'];
                      $get_type=pg_query($dbconn,$get_type);
                      $get_type=pg_fetch_assoc($get_type);
                    echo '
                        <div class="settings__form-group">
                         <label for="file" class="col-sm-2 control-label">Файл  </label>
                        <input id="sortpicture" type="file" class="settings__form-file" name="sortpic" />
                        </div>
                        <div class="settings__inf">
                          <p> Имя </p>
                            <input type="text" class="settings__inf-name" id="name" name="name" value="'.$get_partner['name_children'].'">
                            <p> Описание </p>
                              <textarea name="text" id="text" rows="8" >'.$get_partner['text_children'].'</textarea>
                              <p> Диагноз </p>
                                <textarea name="diagnoz" id="diagnoz" rows="5" >'.$get_partner['diagnosis_children'].'</textarea>
                              <p> <label for="start">Сумма</label>
                                <input type="text"  class="settings__inf-cost" id="cost"  name="cost" value="'.$get_partner['cost_children'].'">
                                <label for="start">Собрали</label>
                                <input type="text" class="settings__inf-collection" id="collection" name="collection" value="'.$get_partner['collection_children'].'">
                              </p>
                              <select name="type" class="type" placeholder="Выбирите статус"  id="type" name="settings__inf-type">
                              <option value="'.$get_type['text_type'].'" disabled selected style="display:none">'.$get_type['text_type'].' </option>
                                  <option value="Нужна помощь">Нужна помощь</option>
                                  <option value="Помогли">Помогли</option>
                                </select>
                              Файлы:
                              <div class="doc_add">
                              <p> Название документа <input name="userfile[]" id="userfile" type="file" multiple />  </p>
                                  <input multiple type="text" class="settings__inf-name" id="name_doc" name="name" value="'.$get_partner['name_doc_children'].'">
                               </div>


                      ';

                        }

                      }
                        else {
                          if($_SESSION['metod']=='news')
                        {

                        echo '
                            <div class="settings__form-group">
                             <label for="file" class="col-sm-2 control-label">Файл  </label>
                            <input id="sortpicture" type="file" class="settings__form-file" name="sortpic" />
                            </div>
                            <div class="settings__inf">
                              <p> Название новости </p>
                                <input type="text" class="settings__inf-name" id="name" name="name" value="">
                              <p> Описание новости </p>
                                <textarea name="text" id="text" rows="8" ></textarea>
                              <label for="start">Дата события</label>
                                <input class="settings__inf-date" type="date" id="date" name="date" value="">
                            </div>
                          ';

                            }
                            if($_SESSION['metod']=='team')
                          {

                          echo '
                              <div class="settings__form-group">
                               <label for="file" class="col-sm-2 control-label">Файл  </label>
                              <input id="sortpicture" type="file" class="settings__form-file" name="sortpic" />
                              </div>
                              <div class="settings__inf">
                                <p> Имя Сотрудника </p>
                                  <input type="text" class="settings__inf-name" id="name" name="name" value="">
                                <p> Описание сотрудника </p>
                                  <textarea name="text" id="text" rows="8" ></textarea>

                              </div>
                            ';

                              }
                              if($_SESSION['metod']=='partner')
                            {

                            echo '
                                <div class="settings__form-group">
                                 <label for="file" class="col-sm-2 control-label">Файл  </label>
                                <input id="sortpicture" type="file" class="settings__form-file" name="sortpic" />
                                </div>
                                <div class="settings__inf">
                                  <p> Название </p>
                                    <input type="text" class="settings__inf-name" id="name" name="name" value="">


                                </div>
                              ';

                                }
                                if($_SESSION['metod']=='report')
                              {

                              echo '
                              <div class="settings__form-group">
                              <p> Описание </p>
                                <textarea name="text" id="text" rows="8" ></textarea>


                                  </div>
                                ';

                                  }
                                  if($_SESSION['metod']=='report_doc')
                                {

                                echo '
                                    <div class="settings__form-group">
                                     <label for="file" class="col-sm-2 control-label">Файл  </label>
                                    <input id="sortpicture" type="file" class="settings__form-file" name="sortpic" >
                                    </div>
                                    <div class="settings__inf">
                                      <p> Название </p>
                                        <input type="text" class="settings__inf-name" id="name" name="name" value="">


                                    </div>
                                  ';

                                    }
                                    if($_SESSION['metod']=='children')
                                  {
                                ;
                                  echo '
                                      <div class="settings__form-group">
                                       <label for="file" class="col-sm-2 control-label">Файл  </label>
                                      <input id="sortpicture" type="file" class="settings__form-file" name="sortpic" >
                                      </div>
                                      <div class="settings__inf">
                                        <p> Имя </p>
                                          <input type="text" class="settings__inf-name" id="name" name="name" value="">
                                          <p> Описание </p>
                                            <textarea name="text" id="text" rows="8" ></textarea>
                                            <p> Диагноз </p>
                                              <textarea name="diagnoz" id="diagnoz" rows="5" ></textarea>
                                            <p> <label for="start">Сумма</label>
                                              <input type="text"  class="settings__inf-cost" id="cost"  name="cost" value="">
                                              <label for="start">Собрали</label>
                                              <input type="text" class="settings__inf-collection" id="collection" name="collection" value="">
                                            </p>
                                            <select name="type" class="type" placeholder="Выбирите статус"  id="type" name="settings__inf-type">
                                                <option value="Нужна помощь">Нужна помощь</option>
                                                <option value="Помогли">Помогли</option>
                                              </select>
                                              <br/>
                                            Файлы:
                                            <div class="doc_add">
                                            <p> Название документа <input name="userfile[]" id="userfile" type="file" multiple />  </p>
                                                <input type="text" multiple class="settings__inf-name" id="name_doc" name="name" value="">
                                             </div>


                                    ';

                                      }


                        }
                          echo '</div>';


            ?>
            </form>
            <?php
            if($_SESSION['id']!='nan')
            {
              if($_SESSION['metod']=='report')
                {
                  echo '<input type="submit" name="red" id="red_inf_report" value="Редактирование">';
                }
              else  if($_SESSION['metod']=='children')
                  {
                    echo '<input type="submit" name="red" id="red_inf_children" value="Редактирование">';
                  }
                else echo ' <input type="submit" name="red" id="red_inf" value="Редактирование">';

                  echo ' <input type="submit" name="del" id="del_inf" value="Удалить">';
                  }
            else {
              if($_SESSION['metod']=='report')
                {
                  echo ' <br/><input type="submit" name="add" id="add_inf_report" value="Сохранить">';
                }
                else  if($_SESSION['metod']=='children')
                    {
                      echo '<input type="submit" name="add" id="add_inf_сhildren" value="Сохранить">';
                    }
                else
                {
                  echo ' <br/><input type="submit" name="add" id="add_inf" value="Сохранить">';
                }
        }

          ?>

</section>

</body>
</html>
