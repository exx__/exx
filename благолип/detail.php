<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<title>фонд</title>
	<?php
         	require ('php/head.php');
    ?>
</head>

<body>



<?php
  require ('php/nav.php');
?>
	<section class="detail">
		<img class="detail__block-img" src="img/phptp-children.png" alt="">
		
		<div class="detail__block-info"><!--блок с детальной-->
			<div class="wrap-name">
				<p class="block-info__name">Арсений Николаевич Петренко</p>
				<a class="wrap-name__link" href="#">Назад</a><!--кнопка назад-->
			</div>
			
 			<p class="block-info__diagnosis">Диагноз:</p>
 			<p class="block-info__info">Воспалительный процесс неясной этиологии спинного мозга, паралитический синдром </p>
 			<p class="block-info__need">Требуется собрать:</p>
 			<p class="block-info__sum">600 000 руб</p>
 			
 			<div class="block-info__prace"><!--нижний блок с ценой-->
 			
 				<div class="prace-green">
 					<p class="block-info__need">Собрано:</p>
 					<p class="block-info__sum color-green">200 000 руб</p>
 				</div>
 				
 				<div class="prace-green">
 					<p class="block-info__need">Осталось собрать:</p>
 					<p class="block-info__sum color-blue">200 000 руб</p>
 				</div>
 			</div>
 			
 			<div class="block-info__btns"><!--блок с кнопками-->
 				<a class="btns__help" href="#">Помочь!</a>
 				<a class="btns__docs" href="#">Счест за реабилитацию</a>
 				<a class="btns__docs" href="#">Еще какой-то документ</a>
 			</div> <!--конец блока с детальной-->
		</div>

	</section>
	
	<section class="history">
		История болезни и семьи ребенка. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet nisl suscipit adipiscing bibendum. Adipiscing enim eu turpis egestas pretium. Mattis aliquam faucibus purus in. Nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque. Sed cras ornare arcu dui vivamus arcu felis bibendum. Phasellus faucibus scelerisque eleifend donec pretium vulputate. Diam donec adipiscing tristique risus nec feugiat. Neque sodales ut etiam sit. Pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus. Mi bibendum neque egestas congue quisque. Diam maecenas ultricies mi eget mauris pharetra et ultrices neque.
	</section>


<?php
     require ('php/footer.php');
?>



	<script src="js/main.js"></script>
</body>

</html>