<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
	<?php
         	require ('php/head.php');
    ?>
</head>
<body>
   
   	<?php
         	require ('php/nav.php');
    ?>
    
    <section class="right-bar">
        <h1 class="right-bar__title">Новый теоритический материал</h1>
        
        <div class="right-bar__top-btn">
            <a class="top-btn__ferst" href="#">Ссылка</a>
            <a class="top-btn__second active-btns" href="#">Ручнной ввод</a>
        </div>
        
        <p class="right-bar__subinp">Введите заголовок теоритического материала</p>
        <input class="right-bar__inp" type="text" >
        
        <p class="right-bar__subinp">Введите текст</p>
        <textarea class="right-bar__textarea" type="text" ></textarea>

        <input class="right-bar__botton-ex" type="button" value="Разместить материал">
    </section>
</body>
</html>