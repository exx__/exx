<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/main.js">
	<?php
         	require ('php/head.php');
    ?>
</head>
<body>
   
   	<?php
         	require ('php/nav.php');
    ?>
    
    <section class="right-bar">
        <h1 class="right-bar__title">Статистика</h1>

        <div class="stat">
            <select class="select__option">
              <option>Пункт 1</option>
              <option>Пункт 2</option>
            </select>
            
            <select class="select__option">
              <option>Пункт 1</option>
              <option>Пункт 2</option>
            </select>
            
            <a class="top-btn__second active-btns" href="#">Нарисовать</a>
        </div>

    </section>
</body>
</html>