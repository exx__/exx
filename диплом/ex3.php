<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/main.js">
	<?php
         	require ('php/head.php');
    ?>
</head>
<body>
   
   	<?php
         	require ('php/nav.php');
    ?>
    
    <section class="right-bar">
        <h1 class="right-bar__title">Оценка экзамена</h1>
        
        <div class="right-bar__block-title">
            <p class="block-title__num">№</p>
            <p class="block-title__name">Имя студета</p>
            <p class="block-title__ex">Пример</p>
            <p class="block-title__rewen">Решение</p>
            <p class="block-title__bal">Баллы</p>
            <p class="block-title__status">Проверено</p>
        </div>
        
        <div class="right-bar__block-info">
            <p class="block-info__num">1</p>
            <p class="block-info__name">Фамилия Имя Отчество</p>
            <p class="block-info__ex">Доказать, что 5^ (k+3) +11^(3k+1) при произвольном целом k>0 делится на 17 без остатка.</p>
            <p class="block-info__rewen"><img src="img/ex-rew.png" alt=""></p>
            <p class="block-info__bal">0/99</p>
            <p class="block-info__status">Проверено</p>
        </div>
    </section>
</body>
</html>