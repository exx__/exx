<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/main.js">
	<?php
         	require ('php/head.php');
    ?>
</head>
<body>
   
   	<?php
         	require ('php/nav.php');
    ?>
    
    <section class="right-bar">
        <h1 class="right-bar__title">Теория</h1>

        <a class="top-btn__second active-btns" href="#">Добавить новую</a>
        
        <a href="#" class="right-bar__teor" >
            <p class="teor__name" download >Метод математической индукции.</p>
            <div class="wrap__redact-btn">
                <input class="teor__pen" type="button">
                <input class="teor__krest" type="button">
            </div>
        </a>

    </section>
</body>
</html>