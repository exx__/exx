<section class="left-menu">
    <img class="menu__logo-site" src="../img/logo-site.svg" alt="">
    <hr>
    <div class="menu__block-user">
        <a href="index.php"><img src="../img/logo-user.svg" alt=""></a>
        <div class="block-user__info">
            <p class="user__info__fio">Фамилия Имя Отчество</p>
            <p class="user__info__status">Студент</p>
        </div>
    </div>
    <hr>
    <nav>
        <ul class="nav-menu">
            <li class="menu__list"><a href="#">Теория</a></li>
            <li class="menu__list"><a href="#">Тренировка</a></li>
            <li class="menu__list"><a href="#">Экзамен</a></li>
            <li class="menu__list"><a href="#">Личный кабинет</a></li>
        </ul>
    </nav>
</section>