<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/main.js">
	<?php
         	require ('php/head.php');
    ?>
</head>
<body>
   
   	<?php
         	require ('php/nav.php');
    ?>
    
    <section class="right-bar">
        <h1 class="right-bar__title">Экзамен</h1>
        <textarea class="right-bar__text" name="" id="" ></textarea>
        <div class="right-bar__btns">
            <input class="btns__ferst" type="button" value="Отметить решенным">
            <input class="btns__second" type="button" value="Отправить решение сейчас">
        </div>
    </section>
</body>
</html>